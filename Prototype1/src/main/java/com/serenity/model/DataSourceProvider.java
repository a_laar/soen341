package com.serenity.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;

import javax.sql.DataSource;

import com.serenity.bean.ApplicationContextProvider;

public class DataSourceProvider {

	public static DataSource getDataSource() {
		return (DataSource)ApplicationContextProvider.getApplicationContext().getBean("dataSource");
	}

	public static ResultSet executeQuery(String query) {
		try
		{			
			Connection c = getDataSource().getConnection();
			Statement s = c.createStatement();

			return s.executeQuery(query);
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
	}

	private static PreparedStatement prepareStatement(String query, Object... args) {
		
		PreparedStatement ps;
		try
		{
			Connection c = DataSourceProvider.getDataSource().getConnection();
			ps = c.prepareStatement(query);
			
			int i = 0;
			for (Object arg : args) {
				
				if (arg == null)
					ps.setNull(i + 1, java.sql.Types.INTEGER);
				else if (arg instanceof Boolean)
					ps.setBoolean(i + 1, (Boolean)arg);
				else if (arg instanceof Integer)
					ps.setInt(i + 1, (Integer)arg);
				else if (arg instanceof Time)
					ps.setTime(i + 1, (Time)arg);
				else
					ps.setString(i + 1, arg.toString());
				
				++i;
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return ps;
	}
	
	public static ResultSet executeQuery(String query, Object... args) {
		try
		{			
			return prepareStatement(query, args).executeQuery();
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
	}
	
	public static int executeUpdate(String query) {
		try
		{			
			Connection c = getDataSource().getConnection();
			Statement s = c.createStatement();

			return s.executeUpdate(query);
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
	}
	
	public static int executeUpdate(String query, Object... args) {
		try
		{			
			return prepareStatement(query, args).executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
	}
	
	public static Integer getInteger(ResultSet rs, String columnLabel) {
		try
		{
			int value = rs.getInt(columnLabel);
			return (value == 0 && rs.wasNull() ? null : value);
		} catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
		}		
	}
}
