package com.serenity.model.data_management;

import java.util.List;

import com.serenity.model.data_management.DataManager.SEEntryType;

public class SEProgram {

	private List<Course> core;
	private List<Course> COMP;
	private List<Course> science;
	private List<Course> CG;
	private List<Course> WSA;
	private List<Course> REA;
	private List<Course> elective;

	public List<Course> getCore() {
		if (this.core == null)
			this.core = DataManager.getSEEntriesFor(SEEntryType.Core);
		return this.core;
	}

	public List<Course> getCOMP() {
		if (this.COMP == null)
			this.COMP = DataManager.getSEEntriesFor(SEEntryType.COMP);
		return this.COMP;
	}

	public List<Course> getScience() {
		if (this.science == null)
			this.science = DataManager.getSEEntriesFor(SEEntryType.Science);
		return this.science;
	}

	public List<Course> getCG() {
		if (this.CG == null)
			this.CG = DataManager.getSEEntriesFor(SEEntryType.CG);
		return this.CG;
	}

	public List<Course> getWSA() {
		if (this.WSA == null)
			this.WSA = DataManager.getSEEntriesFor(SEEntryType.WSA);
		return this.WSA;
	}

	public List<Course> getREA() {
		if (this.REA == null)
			this.REA = DataManager.getSEEntriesFor(SEEntryType.REA);
		return this.REA;
	}

	public List<Course> getElective() {
		if (this.elective == null)
			this.elective = DataManager.getSEEntriesFor(SEEntryType.Elective);
		return this.elective;
	}
}
