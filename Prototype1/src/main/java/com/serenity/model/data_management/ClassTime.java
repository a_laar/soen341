package com.serenity.model.data_management;

import java.util.Calendar;

public class ClassTime {
	
	private Integer id;
	private int idSection;
	private int days;
	private Calendar startTime;
	private Calendar endTime;
	private Section section;

	private static int boolToInt(boolean b) {
		return b ? 1 : 0;
	}
	
	public static int generateDays(boolean monday, boolean tuesday, boolean wednesday, boolean thursday,
			boolean friday, boolean saturday, boolean sunday) {
		return  (boolToInt(monday) << 0)
				+ (boolToInt(tuesday) << 1)
				+ (boolToInt(wednesday) << 2)
				+ (boolToInt(thursday) << 3)
				+ (boolToInt(friday) << 4)
				+ (boolToInt(saturday) << 5)
				+ (boolToInt(sunday) << 6);
	}
	
	public ClassTime(Integer id, int idSection, int days, Calendar startTime, Calendar endTime) {
		if (id == null || id > 0)
			this.setId(id);
		this.setIdSection(idSection);
		this.setDays(days);
		this.setStartTime(startTime);
		this.setEndTime(endTime);
		this.section = null;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id == null || id <= 0)
			this.id = null;
		else
			this.id = id;
	}

	public int getIdSection() {
		return idSection;
	}

	public void setIdSection(int idSection) {
		this.idSection = idSection;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public Calendar getStartTime() {
		return startTime;
	}

	public void setStartTime(Calendar startTime) {
		this.startTime = startTime;
	}

	public Calendar getEndTime() {
		return endTime;
	}

	public void setEndTime(Calendar endTime) {
		this.endTime = endTime;
	}
	
	public boolean getMonday() {
		return (this.days & 1) != 0; 
	}
	
	public boolean getTuesday() {
		return (this.days & 2) != 0; 
	}
	
	public boolean getWednesday() {
		return (this.days & 4) != 0; 
	}
	
	public boolean getThursday() {
		return (this.days & 8) != 0; 
	}
	
	public boolean getFriday() {
		return (this.days & 16) != 0; 
	}
	
	public boolean getSaturday() {
		return (this.days & 32) != 0; 
	}
	
	public boolean getSunday() {
		return (this.days & 64) != 0; 
	}
	
	public Section getSection() {
		if (this.section == null)
			this.section = DataManager.getSectionById(this.idSection);
		return this.section;
	}
}
