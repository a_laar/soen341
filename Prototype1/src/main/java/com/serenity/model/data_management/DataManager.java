package com.serenity.model.data_management;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.serenity.model.DataSourceProvider;

public class DataManager {

	public enum CourseRelation {
		Prerequisite (0),
		Corequisite (1),
		Equivalence (2);
		
		private int value;
		
		private CourseRelation(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return this.value;
		}
	}
	
	public enum SEEntryType {
		Core(0),
		COMP(1),
		Science(2),
		CG(3),
		WSA(4),
		REA(5),
		Elective(6);

		private int value;

		private SEEntryType(int value) {
			this.value = value;
		}

		public int getValue() {
			return this.value;
		}
	}
	
	public static List<Course> getAllCourses() {
		List<Course> sent = new ArrayList<Course>();

		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT id, name, credit FROM courses");
			
			while (rs.next()) {
				sent.add(new Course(rs.getInt("id"), rs.getString("name"), rs.getInt("credit")));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}

	public static Course getCourseById(int id) {
		Course sent = null;
		try
		{
			ResultSet rs = DataSourceProvider.executeQuery("SELECT id, name, credit FROM courses WHERE id = ?", id);
			
			if (rs.next()) {				
				sent = new Course(rs.getInt("id"), rs.getString("name"), rs.getInt("credit"));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	private static boolean courseExists(Course course) {
		
		if (course.getId() == null)
			return false;

		try
		{			
			return DataSourceProvider.executeQuery("SELECT true FROM courses WHERE id = ?", course.getId()).next();
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
	}
	
	public static void addCourse(Course course) {
		if (courseExists(course)) {
			updateCourse(course);
		}
		else {
			insertCourse(course);
		}
	}

	private static void insertCourse(Course course) {
		DataSourceProvider.executeUpdate("INSERT INTO courses (name, credit) VALUES	 (?, ?)",
				course.getName(), course.getCredit());
	}

	private static void updateCourse(Course course) {
		DataSourceProvider.executeUpdate("UPDATE courses SET name=?, credit=? WHERE id=?",
				course.getName(), course.getCredit(), course.getId());
	}
	
	public static void deleteCourse(int id) {
		DataSourceProvider.executeUpdate("DELETE FROM record_entries WHERE id_course = ?", id);
		DataSourceProvider.executeQuery("DELETE FROM se_entries WHERE id_course = ?", id);
		DataSourceProvider.executeUpdate("DELETE FROM sections WHERE id_course = ?", id);
		DataSourceProvider.executeUpdate("DELETE FROM course_relations WHERE id_course_left = ? OR id_course_right = ?", id, id);
		DataSourceProvider.executeUpdate("DELETE FROM courses WHERE id = ?", id);
	}
	
	public static List<Course>getRelations(Course course, CourseRelation relation) {
		List<Course> sent = new ArrayList<Course>();
		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT courses.id, name, credit FROM courses JOIN course_relations ON course_relations.id_course_right = courses.id WHERE course_relations.type = ? AND course_relations.id_course_left = ?", relation.getValue(), course.getId());
			
			while (rs.next()) {				
				sent.add(new Course(rs.getInt("id"), rs.getString("name"), rs.getInt("credit")));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	public static List<Course>getPrerequisites(Course course) {
		return getRelations(course, CourseRelation.Prerequisite);
	}
	
	public static List<Course>getCorequisites(Course course) {
		return getRelations(course, CourseRelation.Corequisite);
	}
	
	public static List<Course>getEquivalences(Course course) {
		return getRelations(course, CourseRelation.Equivalence);
	}
	
	public static void addRelation(CourseRelation relation, Course left, Course right) {
		if (courseExists(left) && courseExists(right) && left.getId() != right.getId() && !relationContains(relation, left, right)) {
			DataSourceProvider.executeUpdate("INSERT INTO course_relations (type, id_course_left, id_course_right) VALUES(?, ?, ?)",
					relation.getValue(), left.getId(), right.getId());
		}
	}

	private static boolean relationContains(CourseRelation relation, Course left, Course right) {
		try
		{
			return DataSourceProvider.executeQuery("SELECT true FROM course_relations WHERE type = ? AND id_course_left = ? AND id_course_right = ?",
					relation.getValue(), left.getId(), right.getId()).next();
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
	}

	public static void deleteRelation(CourseRelation relation, Course left, Course right) {
		DataSourceProvider.executeUpdate("DELETE FROM course_relations WHERE type = ? AND id_course_left = ? AND id_course_right = ?",
				relation.getValue(), left.getId(), right.getId());
	}
	
	public static List<Professor> getAllProfessors() {
		List<Professor> sent = new ArrayList<Professor>();
		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT id, name, is_engineer FROM professors");
			
			while (rs.next()) {				
				sent.add(new Professor(rs.getInt("id"), rs.getString("name"), rs.getBoolean("is_engineer")));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	public static Professor getProfessorById(int id) {
		Professor sent = null;
		
		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT id, name, is_engineer FROM professors WHERE id = ?", id);
			
			if (rs.next()) {
				sent = new Professor(rs.getInt("id"), rs.getString("name"), rs.getBoolean("is_engineer"));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	public static void addProfessor(Professor p) {
		if (professorExists(p))
			DataSourceProvider.executeUpdate("UPDATE professors SET name=?, is_engineer=? WHERE id=?", p.getName(), p.getIsEngineer(), p.getId());
		else
			DataSourceProvider.executeUpdate("INSERT INTO professors (name, is_engineer) VALUES (?, ?)", p.getName(), p.getIsEngineer());
	}

	private static boolean professorExists(Professor p) {
		if (p.getId() == null)
			return false;
		try
		{			
			boolean sent = DataSourceProvider.executeQuery("SELECT true FROM professors WHERE id = ?", p.getId()).next();
			System.out.println("Exists? " + sent);
			return sent;
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
	}
	
	public static void deleteProfessor(int id) {
		DataSourceProvider.executeQuery("UPDATE sections SET id_professor = NULL WHERE id_professor = ?", id);
		DataSourceProvider.executeUpdate("DELETE FROM professors WHERE id = ?", id);
	}
	
	public static List<Section> getCourseSections(Course c) {
		List<Section> sent = new ArrayList<Section>();
		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT id, id_course, name, session, id_professor FROM sections WHERE id_course=?", c.getId());
			
			while (rs.next()) {				
				sent.add(new Section(rs.getInt("id"), rs.getInt("id_course"), rs.getString("name"), rs.getInt("session"), DataSourceProvider.getInteger(rs, "id_professor")));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}

	public static List<Section> getProfessorSections(Professor p) {
		List<Section> sent = new ArrayList<Section>();
		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT id, id_course, name, session, id_professor FROM sections WHERE id_professor = ?", p.getId());
			
			while (rs.next()) {				
				sent.add(new Section(rs.getInt("id"), rs.getInt("id_course"), rs.getString("name"), rs.getInt("session"), DataSourceProvider.getInteger(rs, "id_professor")));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}

	public static Section getSectionById(int id) {
		Section sent = null;
		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT id, id_course, name, session, id_professor FROM sections WHERE id = ?", id);
			
			if (rs.next()) {				
				sent = new Section(rs.getInt("id"), rs.getInt("id_course"), rs.getString("name"), rs.getInt("session"), rs.getInt("id_professor"));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	private static boolean sectionExists(Section s) {
		if (s.getId() == null)
			return false;

		try
		{			
			return DataSourceProvider.executeQuery("SELECT true FROM sections WHERE id = ?", s.getId()).next();
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
	}
	
	public static void addSection(Section s) {
		if (sectionExists(s))
			DataSourceProvider.executeUpdate("UPDATE sections SET id_course=?, name=?, session=?, id_professor=? WHERE id=?", s.getIdCourse(), s.getName(), s.getSession(), s.getIdProfessor(), s.getId());
		else
			DataSourceProvider.executeUpdate("INSERT INTO sections (id_course, name, session, id_professor) VALUES(?, ?, ?, ?)", s.getIdCourse(), s.getName(), s.getSession(), s.getIdProfessor());
	}
	
	public static void deleteSection(int id) {
		DataSourceProvider.executeUpdate("DELETE FROM class_times WHERE id_section = ?", id);
		DataSourceProvider.executeUpdate("DELETE FROM sections WHERE id = ?", id);
	}
	
	private static Calendar timeToCalendar(Time t) {
		Calendar sent = Calendar.getInstance();
		sent.setTimeInMillis(t.getTime());
		return sent;
	}
	
	private static Time calendarToTime(Calendar c) {
		return new Time(c.getTimeInMillis());
	}
	
	public static List<ClassTime> getSectionClassTimes(Section s) {
		List<ClassTime> sent = new ArrayList<ClassTime>();
		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT id, id_section, days, start_time, end_time FROM class_times WHERE id_section = ?", s.getId());
			
			while (rs.next()) {				
				sent.add(new ClassTime(rs.getInt("id"), rs.getInt("id_section"), rs.getInt("days"), timeToCalendar(rs.getTime("start_time")), timeToCalendar(rs.getTime("end_time"))));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	private static boolean classTimeExists(ClassTime c) {
		if (c.getId() == null)
			return false;

		try
		{			
			return DataSourceProvider.executeQuery("SELECT true FROM class_times WHERE id = ?", c.getId()).next();
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
	}
	
	public static void addClassTime(ClassTime c) {
		if (classTimeExists(c))
			DataSourceProvider.executeUpdate("UPDATE class_times SET id_section=?, days=?, start_time=?, end_time=? WHERE id=?", c.getIdSection(), c.getDays(), calendarToTime(c.getStartTime()), calendarToTime(c.getEndTime()), c.getId());
		else
			DataSourceProvider.executeUpdate("INSERT INTO class_times (id_section, days, start_time, end_time) VALUES (?, ?, ?, ?)", c.getIdSection(), c.getDays(), calendarToTime(c.getStartTime()), calendarToTime(c.getEndTime()));
	}
	
	public static void deleteClassTime(int id) {
		DataSourceProvider.executeUpdate("DELETE FROM class_times WHERE id = ?", id);
	}

	public static ClassTime getClassTimeById(int id) {
		ClassTime sent = null;
		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT id, id_section, days, start_time, end_time FROM class_times WHERE id = ?", id);
			
			if (rs.next()) {				
				sent = new ClassTime(rs.getInt("id"), rs.getInt("id_section"), rs.getInt("days"), timeToCalendar(rs.getTime("start_time")), timeToCalendar(rs.getTime("end_time")));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	public static List<Course> getSEEntriesFor(SEEntryType type) {
		List<Course> sent = new ArrayList<Course>();
		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT courses.id, name, credit, session FROM courses JOIN se_entries ON se_entries.id_course = courses.id WHERE se_entries.type = ?", type.getValue());
			
			while (rs.next()) {				
				sent.add(new Course(rs.getInt("id"), rs.getString("name"), rs.getInt("credit")));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	public static void addSEEntry(SEEntryType type, Course c) {
		if (courseExists(c)) {
			DataSourceProvider.executeUpdate("INSERT INTO se_entries (type, id_course) VALUES (?, ?)", type.getValue(), c.getId());
		}
	}
	
	public static void deleteSEEntry(SEEntryType type, Course c) {
		if (courseExists(c))	
			DataSourceProvider.executeUpdate("DELETE FROM se_entries WHERE type = ? AND id_course = ?", type.getValue(), c.getId());
	}
}
