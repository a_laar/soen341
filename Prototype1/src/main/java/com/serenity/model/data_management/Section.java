package com.serenity.model.data_management;

import java.util.List;

public class Section {
	
	private Integer id;
	private int idCourse;
	private String name;
	private int session;
	private Integer idProfessor;
	private Course course;
	private Professor professor;
	private List<ClassTime> classTimes;
	
	public Section(Integer id, int idCourse, String name, int session, Integer idProfessor) {
		if (id == null || id > 0)
			this.id = id;
		this.idCourse = idCourse;
		this.name = name;
		this.session = session;
		if (idProfessor == null || idProfessor > 0)
			this.idProfessor = idProfessor;
		this.classTimes = null;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Section)
			return this.id == ((Section)obj).id && this.idCourse == ((Section)obj).idCourse && this.name == ((Section)obj).name;
		return super.equals(obj);
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id == null || id <= 0)
			this.id = null;
		else
			this.id = id;
	}

	public int getIdCourse() {
		return idCourse;
	}

	public void setIdCourse(int idCourse) {
		this.idCourse = idCourse;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getSession() {
		return session;
	}
	
	public void setSession(int session) {
		this.session = session;
	}

	public Integer getIdProfessor() {
		return idProfessor;
	}

	public void setIdProfessor(Integer idProfessor) {
		if (id == null || idProfessor <= 0)
			this.idProfessor = null;
		else
			this.idProfessor = idProfessor;
	}
	
	public Course getCourse() {
		if (this.course == null)
			this.course = DataManager.getCourseById(this.idCourse);
		return this.course;
	}
	
	public Professor getProfessor() {
		if (this.idProfessor != null && this.professor == null)
			this.professor = DataManager.getProfessorById(this.idProfessor);
		return this.professor;
	}
	
	public List<ClassTime> getClassTimes() {
		if (this.classTimes == null)
			this.classTimes = DataManager.getSectionClassTimes(this);
		return this.classTimes;
	}
}
