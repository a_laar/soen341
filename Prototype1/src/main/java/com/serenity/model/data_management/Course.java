package com.serenity.model.data_management;

import java.util.List;

public class Course {
	
	private Integer id;
	private String name;
	private int credit;
	private List<Course> prerequisites;
	private List<Course> corequisites;
	private List<Course> equivalences;
	private List<Section> sections;

	public Course(Integer id, String name, int credit) {
		if (id == null || id > 0)
			this.id = id;
		this.name = name;
		this.credit = credit;
		this.prerequisites = null;
		this.corequisites = null;
		this.equivalences = null;
		this.sections = null;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Course)
			return this.id == ((Course)obj).id && this.name == ((Course)obj).name;
		return super.equals(obj);
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (id == null || id <= 0)
			this.id = null;
		else
			this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getCredit() {
		return credit;
	}
	
	public void setCredit(int credit) {
		this.credit = credit;
	}

	public List<Course> getPrerequisites() {
		if (this.prerequisites == null)
			this.prerequisites = DataManager.getPrerequisites(this);
		return this.prerequisites;
	}

	public List<Course> getCorequisites() {
		if (this.corequisites == null)
			this.corequisites = DataManager.getCorequisites(this);
		return this.corequisites;
	}

	public List<Course> getEquivalences() {
		if (this.equivalences == null)
			this.equivalences= DataManager.getEquivalences(this);
		return this.equivalences;
	}
	
	public List<Section> getSections() {
		if (this.sections == null)
			this.sections = DataManager.getCourseSections(this);
		return this.sections;
	}
 }
