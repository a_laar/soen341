package com.serenity.model.data_management;

import java.util.List;

public class Professor {
	private Integer id;
	private String name;
	private boolean isEngineer;
	private List<Section> sections;

	public Professor(Integer id, String name, boolean isEngineer) {
		if (id == null || id > 0)
			this.id = id;
		this.name = name;
		this.isEngineer = isEngineer;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Professor)
			return this.id == ((Professor)obj).id && this.name == ((Professor)obj).name;
		return super.equals(obj);
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		if (id == null || id <= 0)
			this.id = null;
		else
			this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean getIsEngineer() {
		return isEngineer;
	}
	
	public void setIsEngineer(boolean isEngineer) {
		this.isEngineer = isEngineer;
	}
	
	public List<Section> getSections() {
		if (this.sections == null)
			this.sections = DataManager.getProfessorSections(this);
		return this.sections;
	}
}
