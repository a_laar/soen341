package com.serenity.model.user_management;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.context.SecurityContextHolder;

import com.serenity.model.DataSourceProvider;

public final class UserManager {
	
	public static User getCurrentUser() {
		return getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
	}
	
	public static User getUserByUsername(String username) {
		User sent = null;
		try
		{
			ResultSet rs = DataSourceProvider.executeQuery("SELECT username, email FROM users WHERE enabled = 1 AND username = ?", username);
			
			if (rs.next()) {
				sent = new User(rs.getString("username"), rs.getString("email"),
						DataSourceProvider.executeQuery("SELECT true FROM user_roles WHERE role='ROLE_ADMIN' AND username=?", username).next()
						);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	public static List<User> getAllUsers() {
		List<User> sent = new ArrayList<User>();
		try
		{
			ResultSet rs = DataSourceProvider.executeQuery("SELECT username, email FROM users WHERE enabled = 1");
			
			while (rs.next()) {
				ResultSet roles = DataSourceProvider.executeQuery("SELECT true FROM user_roles WHERE role='ROLE_ADMIN' AND username=?", rs.getString("username"));
				
				sent.add(new User(rs.getString("username"), rs.getString("email"), roles.next()));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	public static List<User> getDisabledUsers() {
		List<User> sent = new ArrayList<User>();
		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT username, email FROM users WHERE enabled = 0");
			
			while (rs.next()) {
				ResultSet roles = DataSourceProvider.executeQuery("SELECT true FROM user_roles WHERE role='ROLE_ADMIN' AND username=?", rs.getString("username"));
				
				sent.add(new User(rs.getString("username"), rs.getString("email"), roles.next()));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}

	public static void disableUser(String username)	{
		DataSourceProvider.executeUpdate("UPDATE users SET enabled=0 WHERE username=?", username);
	}

	public static void enableUser(String username)	{
		DataSourceProvider.executeUpdate("UPDATE users SET enabled=1 WHERE username=?", username);
	}

	public static void deleteUser(String username)	{
		DataSourceProvider.executeUpdate("DELETE FROM students WHERE username=?", username);
		DataSourceProvider.executeUpdate("DELETE FROM user_roles WHERE username=?", username);
		DataSourceProvider.executeUpdate("DELETE FROM users WHERE username=?", username);
	}
	
	public static void addUser(User user, String password) {
		DataSourceProvider.executeUpdate("INSERT INTO users (username, password, email) VALUES (?, sha1(?), ?)",
				user.getUsername(), password, user.getEmail());
		DataSourceProvider.executeUpdate("INSERT INTO user_roles (role, username) VALUES(?, ?)",
				(user.getIsAdmin() ? "ROLE_ADMIN" : "ROLE_STUDENT"), user.getUsername());
	}
	
	public static List<Student> getAllStudent() {
		List<Student> sent = new ArrayList<Student>();

		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT username, name, student_id FROM students");
			
			while (rs.next()) {
				sent.add(new Student(rs.getString("username"), rs.getString("name"), rs.getString("student_id")));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	public static Student getStudentById(String username) {
		Student sent = null;

		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT username, name, student_id FROM students WHERE username=?", username);
			
			if (rs.next()) {
				sent = new Student(rs.getString("username"), rs.getString("name"), rs.getString("student_id"));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	public static boolean studentExists(Student s) {
		try
		{			
			return DataSourceProvider.executeQuery("SELECT true FROM students WHERE username = ?", s.getUsername()).next();
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
	}
	
	public static void addStudent(Student s)
	{
		if (studentExists(s))
			DataSourceProvider.executeUpdate("UPDATE students SET name=?, student_id=? WHERE username=?", s.getName(), s.getStudentId(), s.getUsername());
		else
			DataSourceProvider.executeUpdate("INSERT INTO students (username, name, student_id) VALUES (?, ?, ?)", s.getUsername(), s.getName(), s.getStudentId());
	}
	
	public static void deleteStudent(String username) {
		// TODO delete corresponding schedules
		DataSourceProvider.executeUpdate("DELETE FROM record_entries WHERE id_academic IN (SELECT id FROM academic_records WHERE student_username=?)", username);
		DataSourceProvider.executeUpdate("DELETE FROM academic_records WHERE student_username=?", username);
		DataSourceProvider.executeUpdate("DELETE FROM students WHERE username=?", username);
	}

	public static List<String> getAllNonStudents() {
		List<String> sent = new ArrayList<String>();

		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT users.username FROM users LEFT JOIN students ON users.username = students.username WHERE users.enabled = 1 AND students.username IS NULL");
			
			while (rs.next()) {
				sent.add(rs.getString("username"));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	public static List<AcademicRecord> getStudentsRecords(Student s) {

		List<AcademicRecord> sent = new ArrayList<AcademicRecord>();
		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT id, academic_records.student_username, session, year FROM academic_records JOIN students ON academic_records.student_username = students.username WHERE students.username = ?", s.getUsername());
			
			while (rs.next()) {
				sent.add(new AcademicRecord(rs.getInt("id"), rs.getString("student_username"), rs.getInt("session"), rs.getInt("year")));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	public static AcademicRecord getRecordById(int id) {
		AcademicRecord sent = null;

		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT id, student_username, session, year FROM academic_records WHERE id = ?", id);
			
			while (rs.next()) {
				sent = new AcademicRecord(rs.getInt("id"), rs.getString("student_username"), rs.getInt("session"), rs.getInt("year"));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	public static boolean recordExists(AcademicRecord record) {
		if (record.getId() == null)
			return false;
		try
		{			
			return DataSourceProvider.executeQuery("SELECT true FROM academic_records WHERE id = ?", record.getId()).next();
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
	}
	
	public static void addRecord(AcademicRecord record) {
		if (recordExists(record))
			DataSourceProvider.executeUpdate("UPDATE academic_records SET student_username=?, session=?, year=? WHERE id=?", record.getUsername(), record.getSession(), record.getYear(), record.getId());
		else
			DataSourceProvider.executeUpdate("INSERT INTO academic_records (student_username, session, year) VALUES (?, ?, ?)", record.getUsername(), record.getSession(), record.getYear());
	}
	
	public static void deleteRecord(int id) {
		DataSourceProvider.executeUpdate("DELETE FROM record_entries WHERE id_academic=?", id);
		DataSourceProvider.executeUpdate("DELETE FROM academic_records WHERE id=?", id);
	}
	
	public static List<RecordEntry> getRecordEntries(AcademicRecord record) {
		List<RecordEntry> sent = new ArrayList<RecordEntry>();

		try
		{			
			ResultSet rs = DataSourceProvider.executeQuery("SELECT record_entries.id, id_academic, id_course, grade, credits_earned FROM record_entries JOIN academic_records ON academic_records.id = record_entries.id_academic WHERE academic_records.id = ?", record.getId());
			
			while (rs.next()) {
				sent.add(new RecordEntry(rs.getInt("id"), rs.getInt("id_academic"), rs.getInt("id_course"), rs.getFloat("grade"), rs.getInt("credits_earned")));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
		return sent;
	}
	
	public static boolean recordEntryExists(RecordEntry entry) {
		if (entry.getId() == null)
			return false;
		try
		{			
			return DataSourceProvider.executeQuery("SELECT true FROM record_entries WHERE id = ?", entry.getId()).next();
		}
		catch (SQLException e)
		{
			e.printStackTrace(); 
            throw new RuntimeException(e);
        }
	}
	
	public static void addRecordEntry(RecordEntry entry) {
		if (recordEntryExists(entry))
			DataSourceProvider.executeUpdate("UPDATE record_entries SET id_academic=?, id_course=?, grade=?, credits_earned=? WHERE id=?", entry.getIdAcademic(), entry.getIdCourse(), entry.getGrade(), entry.getCreditsEarned(), entry.getId());
		else
			DataSourceProvider.executeUpdate("INSERT INTO record_entries (id_academic, id_course, grade, credits_earned) VALUES (?, ?, ?, ?)", entry.getIdAcademic(), entry.getIdCourse(), entry.getGrade(), entry.getCreditsEarned());
	}
	
	public static void deleteRecordEntry(int id) {
		DataSourceProvider.executeUpdate("DELETE FROM record_entries WHERE id=?", id);
	}
}
