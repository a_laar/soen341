package com.serenity.model.user_management;

import java.util.List;

public class AcademicRecord {

	private Integer id;
	private String username;
	private int session;
	private int year;
	private List<RecordEntry> entries;

	public AcademicRecord(Integer id, String username, int session, int year) {
		this.id = id;
		this.username = username;
		this.session = session;
		this.year = year;
		this.entries = null;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public int getSession() {
		return session;
	}
	
	public void setSession(int session) {
		this.session = session;
	}
	
	public int getYear() {
		return year;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public int getCredits() {
		int sum = 0;
		
		for (RecordEntry entry : this.getEntries())
			sum += entry.getCreditsEarned();
		return sum;
	}
	
	public float getGPA() {
		int total = 0;
		float sum = 0;
		
		for (RecordEntry entry : this.getEntries())
		{
			sum += (float)entry.getCreditsEarned() * entry.getGrade();
			total += entry.getCreditsEarned();
		}
		return Math.round((total == 0 ? 0 : (sum / (float)total)) * 100.0f) / 100.0f;
	}
	
	public List<RecordEntry> getEntries() {
		if (this.entries == null)
			this.entries = UserManager.getRecordEntries(this);
		return this.entries;
	}
}
