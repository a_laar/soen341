package com.serenity.model.user_management;

public final class User {

	private boolean isAdmin;
	private String username;
	private String email;

	public User(String username, String email, boolean isAdmin) {
		this.username = username;
		this.email = email;
		this.isAdmin = isAdmin;
	}

	public boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String login) {
		this.username = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public Student getStudent() {
		return UserManager.getStudentById(this.username);
	}
}
