package com.serenity.model.user_management;

import com.serenity.model.data_management.Course;
import com.serenity.model.data_management.DataManager;

public class RecordEntry {

	private Integer id;
	private int idAcademic;
	private int idCourse;
	private float grade;
	private int creditsEarned;

	public RecordEntry(Integer id, int idAcademic, int idCourse, float grade, int creditsEarned) {
		this.id = id;
		this.idAcademic = idAcademic;
		this.idCourse = idCourse;
		this.grade = grade;
		this.creditsEarned = creditsEarned;
	}
	
	public AcademicRecord getRecord() {
		return UserManager.getRecordById(this.idAcademic);
	}
	
	public Course getCourse() {
		return DataManager.getCourseById(this.idCourse);
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public int getIdAcademic() {
		return idAcademic;
	}
	
	public void setIdAcademic(int idAcademic) {
		this.idAcademic = idAcademic;
	}
	
	public int getIdCourse() {
		return idCourse;
	}
	
	public void setIdCourse(int idCourse) {
		this.idCourse = idCourse;
	}
	
	public float getGrade() {
		return grade;
	}
	
	public void setGrade(float grade) {
		this.grade = grade;
	}
	
	public int getCreditsEarned() {
		return creditsEarned;
	}
	
	public void setCreditsEarned(int creditsEarned) {
		this.creditsEarned = creditsEarned;
	}
}
