package com.serenity.model.user_management;

import java.util.List;

public class Student {
	
	private String username;
	private String name;
	private String studentId;
	private List<AcademicRecord> academicRecords;

	public Student(String username, String name, String studentId) {
		this.username = username;
		this.name = name;
		this.studentId = studentId;
		this.academicRecords = null;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getStudentId() {
		return studentId;
	}
	
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	
	public List<AcademicRecord> getAcademicRecords() {
		if (this.academicRecords == null)
			this.academicRecords = UserManager.getStudentsRecords(this);
		return this.academicRecords;
	}
}
