package com.serenity.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;

import nz.net.ultraq.thymeleaf.LayoutDialect;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.dialect.IDialect;
import org.thymeleaf.extras.springsecurity3.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ITemplateResolver;

import com.serenity.bean.ApplicationContextProvider;

@EnableWebMvc
@Configuration
@Import({SecurityConfig.class})
public class AppConfig {

	private static String dbPropFilename = "db.properties";
	
	@Bean(name= "dataSource")
	public DriverManagerDataSource dataSource() throws IOException {
		Properties prop = new Properties();
		InputStream propFile = new FileInputStream(dbPropFilename);
		
		prop.load(propFile);

		DriverManagerDataSource sent = new DriverManagerDataSource();
		
		sent.setDriverClassName("com.mysql.jdbc.Driver");

		sent.setUrl("jdbc:mysql://" + prop.getProperty("db.url") + "/serenity");
		sent.setUsername(prop.getProperty("db.username"));
		sent.setPassword(prop.getProperty("db.password"));
//		sent.setUrl("jdbc:mysql://localhost:3306/serenity");
//		sent.setUsername("soen341");
//		sent.setPassword("123456");

		return sent;
	}
	
	@Autowired
	ITemplateResolver thymeleafResolver;

	@Bean(name="templateEngine")
	public TemplateEngine templateEngine() {
		SpringTemplateEngine sent = new SpringTemplateEngine();
		
		sent.setTemplateResolver(thymeleafResolver);
		sent.setAdditionalDialects(new HashSet<IDialect>(Arrays.asList(new LayoutDialect(), new SpringSecurityDialect())));
		return sent;
	}

	@Bean(name="applicationContextProvider")
	public ApplicationContextAware applicationContextProvider()
	{
		return new ApplicationContextProvider();
	}
}
