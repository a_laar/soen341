package com.serenity.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;

@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;
	
	ShaPasswordEncoder encoder = new ShaPasswordEncoder();
	
	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource)
			.passwordEncoder(encoder)
			.usersByUsernameQuery("SELECT username, password, enabled FROM users WHERE username=?")
			.authoritiesByUsernameQuery("SELECT username, role FROM user_roles WHERE username=?");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/admin/**", "/user/**", "/course/courses_management/**", "/professor/management/**", "/section/management/**", "/class_time/management/**", "/se_program/management/**", "/academic_record/management/**")
				.access("hasRole('ROLE_ADMIN')")
			.antMatchers("/student/**", "/course/**", "/professor/**", "/section/**", "/class_time/**", "/se_program/**", "/academic_record/**")
				.access("hasRole('ROLE_ADMIN') or hasRole('ROLE_STUDENT')")
			.and().formLogin()
			.and().logout().logoutSuccessUrl("/")
		;
	}
}
