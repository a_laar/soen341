package com.serenity.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.serenity.model.data_management.DataManager;
import com.serenity.model.data_management.Section;

@Controller
public class SectionController {

	@RequestMapping(value={"/section/", "/section/sections"}, method=RequestMethod.GET)
	public String sectionsPage(Model model, @RequestParam("id_course") int id_course) {
		model.addAttribute("course", DataManager.getCourseById(id_course));
		return "/section/sections";
	}
	
	@RequestMapping(value="/section/management/editor", method=RequestMethod.GET)
	public String sectionEditorPage(Model model, @RequestParam(value="id", required=false) Integer id,
			@RequestParam("id_course") int id_course) {
		model.addAttribute("id_course", id_course);
		model.addAttribute("professors", DataManager.getAllProfessors());
		if (id != null)
			model.addAttribute("section", DataManager.getSectionById(id));
		return "/section/edit";
	}
	
	@RequestMapping(value={"/section/management/add", "/section/management/edit"}, method={RequestMethod.GET, RequestMethod.POST})
	public String sectionAdd(RedirectAttributes redirectAttributes, @RequestParam(value="id", required=false) Integer id,
			@RequestParam("id_course") int id_course, @RequestParam("name") String name, @RequestParam("session") int session,
			@RequestParam("id_professor") Integer id_professor) {
		DataManager.addSection(new Section(id, id_course, name, session, id_professor));
		redirectAttributes.addAttribute("id_course", id_course);
		return "redirect:/section/sections";
	}
	
	@RequestMapping(value="/section/management/delete", method={RequestMethod.GET, RequestMethod.POST})
	public String sectionDelete(RedirectAttributes redirectAttributes, @RequestParam(value="id") int id,
			@RequestParam("id_course") int id_course) {
		DataManager.deleteSection(id);
		redirectAttributes.addAttribute("id_course", id_course);
		return "redirect:/section/sections";
	}
}
