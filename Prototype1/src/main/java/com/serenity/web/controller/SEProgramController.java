package com.serenity.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.serenity.model.data_management.DataManager;
import com.serenity.model.data_management.DataManager.SEEntryType;
import com.serenity.model.data_management.SEProgram;

@Controller
public class SEProgramController {

	@RequestMapping("/se_program")
	public String SEProgramPage(Model model) {
		model.addAttribute("se_program", new SEProgram());
		model.addAttribute("all_courses", DataManager.getAllCourses());
		return "/se_program/se_program";
	}

	@RequestMapping(value="/se_program/management/add", method={RequestMethod.POST, RequestMethod.GET})
	public String addEntry(
			@RequestParam("type") int type,
			@RequestParam("id_course") int id_course
			) {
		DataManager.addSEEntry(SEEntryType.values()[type], DataManager.getCourseById(id_course));
		return "redirect:/se_program";
	}
	
	@RequestMapping(value="/se_program/management/delete", method={RequestMethod.POST, RequestMethod.GET})
	public String deleteEntry(
			@RequestParam("type") int type,
			@RequestParam("id_course") int id_course
			) {
		DataManager.deleteSEEntry(SEEntryType.values()[type], DataManager.getCourseById(id_course));
		return "redirect:/se_program";
	}
}
