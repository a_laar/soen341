package com.serenity.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.serenity.model.data_management.DataManager;
import com.serenity.model.data_management.Professor;

@Controller
public class ProfessorController {
	
	@RequestMapping(value={"/professor", "/professor/professors"})
	public String professorsPage(Model model) {
		model.addAttribute("professors", DataManager.getAllProfessors());
		return "professor/professors";
	}
	
	@RequestMapping(value="/professor/management/editor", method=RequestMethod.GET)
	public String professorEditPage(Model model, @RequestParam(value="id", defaultValue="-1") int id) {
		model.addAttribute("professor", (id == -1 ? null : DataManager.getProfessorById(id)));
		return "/professor/edit";
	}
	
	@RequestMapping(value={"/professor/management/add", "/professor/management/edit"}, method={RequestMethod.POST, RequestMethod.GET})
	public String professorAdd(@RequestParam(value="id", required=false) Integer id, @RequestParam("name") String name,
			@RequestParam(value="is_engineer", defaultValue="false") boolean isEngineer) {
		DataManager.addProfessor(new Professor(id, name, isEngineer));
		return "redirect:/professor/professors";
	}
	
	@RequestMapping(value="/professor/management/delete", method={RequestMethod.POST, RequestMethod.GET})
	public String professerDelete(@RequestParam("id") int id) {
		DataManager.deleteProfessor(id);
		return "redirect:/professor/professors";
	}
	
}
