package com.serenity.web.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.serenity.model.data_management.ClassTime;
import com.serenity.model.data_management.DataManager;

@Controller
public class ClassTimeController {

	@RequestMapping(value={"/class_time/", "/class_time/class_times"}, method=RequestMethod.GET)
	public String classTimesPage(Model model, @RequestParam("id_section") int id_section) {
		model.addAttribute("section", DataManager.getSectionById(id_section));
		return "/class_time/class_times";
	}
	
	@RequestMapping(value="/class_time/management/editor", method=RequestMethod.GET)
	public String classTimeEditorPage(Model model, @RequestParam(value="id", required=false) Integer id,
			@RequestParam("id_section") int id_section) {
		model.addAttribute("id_section", id_section);
		if (id != null)
			model.addAttribute("class_time", DataManager.getClassTimeById(id));
		return "/class_time/edit";
	}
	
	private static Calendar stringToCalendar(String source) {
		Calendar sent;
		try {
			sent = Calendar.getInstance();
			sent.setTime(new SimpleDateFormat("HH:mm").parse(source));
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return sent;
	}

	
	@RequestMapping(value={"/class_time/management/add", "/class_time/management/edit"}, method={RequestMethod.GET, RequestMethod.POST})
	public String classTimeAdd(RedirectAttributes redirectAttributes,
			@RequestParam(value="id", required=false) Integer id,
			@RequestParam("id_section") int id_section,
			@RequestParam(value="monday", defaultValue="false") boolean monday,
			@RequestParam(value="tuesday", defaultValue="false") boolean tuesday,
			@RequestParam(value="wednesday", defaultValue="false") boolean wednesday,
			@RequestParam(value="thursday", defaultValue="false") boolean thursday,
			@RequestParam(value="friday", defaultValue="false") boolean friday,
			@RequestParam(value="saturday", defaultValue="false") boolean saturday,
			@RequestParam(value="sunday", defaultValue="false") boolean sunday,
			@RequestParam("start_time") String start_time,
			@RequestParam("end_time") String end_time) {
		DataManager.addClassTime(new ClassTime(id, id_section, ClassTime.generateDays(monday, tuesday, wednesday, thursday, friday, saturday, sunday), stringToCalendar(start_time), stringToCalendar(end_time)));
		redirectAttributes.addAttribute("id_section", id_section);
		return "redirect:/class_time/class_times";
	}
	
	@RequestMapping(value="/class_time/management/delete", method={RequestMethod.GET, RequestMethod.POST})
	public String classTimeDelete(RedirectAttributes redirectAttributes,
			@RequestParam(value="id") int id,
			@RequestParam("id_section") int id_section) {
		DataManager.deleteClassTime(id);
		redirectAttributes.addAttribute("id_section", id_section);
		return "redirect:/class_time/class_times";
	}
}
