package com.serenity.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.serenity.model.user_management.Student;
import com.serenity.model.user_management.UserManager;

@Controller
public class StudentsController {

	@RequestMapping(value={"/student/", "/student/students"})
	public String studentsPage(Model model) {
		model.addAttribute("students", UserManager.getAllStudent());
		model.addAttribute("non_students", UserManager.getAllNonStudents());
		return "/student/students";
	}
	
	@RequestMapping("/student/management/editor")
	public String studentEditorPage(Model model,
			@RequestParam("username") String username
			) {
		model.addAttribute("username", username);
		model.addAttribute("student", UserManager.getStudentById(username));
		return "/student/edit";
	}
	
	@RequestMapping(value={"/student/management/add", "/student/management/edit"}, method={RequestMethod.GET, RequestMethod.POST})
	public String addStudent(
			@RequestParam("username") String username,
			@RequestParam("name") String name,
			@RequestParam("student_id") String student_id
			) {
		UserManager.addStudent(new Student(username, name, student_id));
		return "redirect:/student/students";
	}
	
	@RequestMapping(value="/student/management/delete")
	public String deleteStudent(
			@RequestParam("username") String username
			) {
		UserManager.deleteStudent(username);
		return "redirect:/student/students";
	}
}
