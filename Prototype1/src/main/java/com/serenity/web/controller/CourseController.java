package com.serenity.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.serenity.model.data_management.Course;
import com.serenity.model.data_management.DataManager;
import com.serenity.model.data_management.DataManager.CourseRelation;

@Controller
public class CourseController {
	
	@RequestMapping(value={"/course", "/course/courses"})
	public String coursesPage(Model model) {
		model.addAttribute("courses", DataManager.getAllCourses());
		return "course/courses";
	}

	@RequestMapping("/course/courses_management/add")
	public String addCoursePage(Model model) {
		return editCoursePage(model, null);
	}
	
	@RequestMapping("/course/courses_management/edit")
	public String editCoursePage(Model model, @RequestParam(value="id") Integer id) {
		model.addAttribute("course", id == null ? null : DataManager.getCourseById(id));
		model.addAttribute("all_courses", DataManager.getAllCourses());
		return "course/courses_management/edit_course";
	}
		
	@RequestMapping(value="/course/courses_management/edit_course", method=RequestMethod.POST)
	public String editCourse(Model model, @RequestParam(value="id", required=false) Integer id, @RequestParam("name") String name,
			@RequestParam("credit") int credit) {
		DataManager.addCourse(new Course(id, name, credit));
		return "redirect:/course/courses/";
	}

	@RequestMapping("/course/courses_management/delete")
	public String deleteCourse(Model model, @RequestParam("id") int id) {
		DataManager.deleteCourse(id);
		return "redirect:/course/courses/";
	}

	@RequestMapping(value="/course/courses_management/add_relation", method=RequestMethod.POST)
	public String addRelation(RedirectAttributes redirectAttributes, @RequestParam("relation") int relation, @RequestParam("id_left") int id_left,
			@RequestParam("id_right") int id_right) {
		CourseRelation r = (relation == 0 ? CourseRelation.Prerequisite : (relation == 1 ? CourseRelation.Corequisite : CourseRelation.Equivalence));
		
		DataManager.addRelation(r, DataManager.getCourseById(id_left), DataManager.getCourseById(id_right));
		redirectAttributes.addAttribute("id", id_left);
		return "redirect:/course/courses_management/edit";
	}
	
	@RequestMapping(value="/course/courses_management/delete_relation", method=RequestMethod.GET)
	public String deleteRelation(RedirectAttributes redirectAttributes, @RequestParam("relation") int relation, @RequestParam("id_left") int id_left,
			@RequestParam("id_right") int id_right) {
		CourseRelation r = (relation == 0 ? CourseRelation.Prerequisite : (relation == 1 ? CourseRelation.Corequisite : CourseRelation.Equivalence));
		
		DataManager.deleteRelation(r, DataManager.getCourseById(id_left), DataManager.getCourseById(id_right));
		redirectAttributes.addAttribute("id", id_left);
		return "redirect:/course/courses_management/edit";
	}
}
