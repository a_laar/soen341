package com.serenity.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.serenity.model.user_management.User;
import com.serenity.model.user_management.UserManager;

@Controller
public class UserController {
	
	@RequestMapping(value={"/user", "/user/user_management"})
	public String userManagementPage(Model model) {
		model.addAttribute("users", UserManager.getAllUsers());
		model.addAttribute("disabled_users", UserManager.getDisabledUsers());
		return "user/user_management";
	}
	
	@RequestMapping("/user/disable_user")
	public String disableUser(@RequestParam("username") String username) {
		UserManager.disableUser(username);
		return "redirect:/user/user_managemet";
	}
	
	@RequestMapping("/user/enable_user")
	public String enableUser(@RequestParam("username") String username) {
		UserManager.enableUser(username);
		return "redirect:/user/user_managemet";
	}
	
	@RequestMapping("/user/delete_user")
	public String deleteUser(@RequestParam("username") String username) {
		UserManager.deleteUser(username);
		return "redirect:/user/user_managemet";
	}
	
	@RequestMapping(value="/user/add_user", method=RequestMethod.GET)
	public String addUser()
	{
		return "user/add_user";
	}
	
	@RequestMapping(value="/user/add_user", method=RequestMethod.POST)
	public String addUser(@RequestParam("username") String username, @RequestParam("email") String email,
			@RequestParam("password") String password, @RequestParam(value="isAdmin", defaultValue="false") boolean isAdmin) {
		UserManager.addUser(new User(username, email, isAdmin), password);
		return "redirect:/user/user_managemet";
	}
}
