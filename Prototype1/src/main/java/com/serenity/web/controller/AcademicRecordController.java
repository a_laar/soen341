package com.serenity.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.serenity.model.data_management.DataManager;
import com.serenity.model.user_management.AcademicRecord;
import com.serenity.model.user_management.Student;
import com.serenity.model.user_management.UserManager;

@Controller
@RequestMapping("/academic_record/")
public class AcademicRecordController {

	@RequestMapping(value={"/", "/academic_records"})
	public String listPage(Model model,
			@RequestParam(value="username", required=false) String username
			) {
		Student s = null;
		
		if (username != null)
			s = UserManager.getStudentById(username);
		if (s == null)
			s = UserManager.getCurrentUser().getStudent();
		model.addAttribute("username", s == null ? UserManager.getCurrentUser().getUsername() : s.getUsername());
		model.addAttribute("student", s);
		model.addAttribute("students", UserManager.getAllStudent());
		return "/academic_record/list";
	}
	
	@RequestMapping(value="/management/editor")
	public String editorPage(Model model,
			@RequestParam("username") String username,
			@RequestParam(value="id", required=false) Integer id
			) {
		model.addAttribute("username", username);
		if (id != null)
		{
			model.addAttribute("record", UserManager.getRecordById(id));
			model.addAttribute("courses", DataManager.getAllCourses());
		}
		return "/academic_record/editor";
	}
	
	@RequestMapping(value={"/management/add", "/management/edit"}, method={RequestMethod.GET, RequestMethod.POST})
	public String addRecord(RedirectAttributes redirectAttributes,
			@RequestParam(value="id", required=false) Integer id,
			@RequestParam("username") String username,
			@RequestParam("session") int session,
			@RequestParam("year") int year
			) {
		UserManager.addRecord(new AcademicRecord(id, username, session, year));
		redirectAttributes.addAttribute("username", username);
		return "redirect:/academic_record/academic_records";
	}
	
	@RequestMapping(value={"/management/delete"}, method={RequestMethod.GET, RequestMethod.POST})
	public String deleteRecord(RedirectAttributes redirectAttributes,
			@RequestParam("id") int id,
			@RequestParam("username") String username
			) {
		UserManager.deleteRecord(id);
		redirectAttributes.addAttribute("username", username);
		return "redirect:/academic_record/academic_records";
	}
}
