package com.serenity.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.serenity.model.user_management.UserManager;

@Controller
public class MainController {

	@RequestMapping(value={"/", "/hello"})
	public String defaultPage(Model model) {
		model.addAttribute("current_user", UserManager.getCurrentUser());
		return "hello";
	}
	
	@RequestMapping("/student")
	public String studentPage() {
		return "admin";
	}
	
	@RequestMapping("/admin")
	public String adminPage() {
		return "admin";
	}
}
