package com.serenity.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.serenity.model.user_management.RecordEntry;
import com.serenity.model.user_management.UserManager;

@Controller
@RequestMapping("/record_entry/")
public class RecordEntryController {
	
	
	@RequestMapping(value={"/management/add", "/management/edit"}, method={RequestMethod.GET, RequestMethod.POST})
	public String addRecord(RedirectAttributes redirectAttributes,
			@RequestParam("username") String username,
			@RequestParam(value="id", required=false) Integer id,
			@RequestParam("id_academic") int id_academic,
			@RequestParam("id_course") int id_course,
			@RequestParam("grade") float grade,
			@RequestParam("credits_earned") int credits_earned
			) {
		UserManager.addRecordEntry(new RecordEntry(id, id_academic, id_course, grade, credits_earned));
		redirectAttributes.addAttribute("username", username);
		redirectAttributes.addAttribute("id", id_academic);
		return "redirect:/academic_record/management/editor";
	}
	
	@RequestMapping(value={"/management/delete"}, method={RequestMethod.GET, RequestMethod.POST})
	public String deleteRecord(RedirectAttributes redirectAttributes,
			@RequestParam("id") int id,
			@RequestParam("id_academic") int id_academic,
			@RequestParam("username") String username
			) {
		UserManager.deleteRecordEntry(id);
		redirectAttributes.addAttribute("username", username);
		redirectAttributes.addAttribute("id", id_academic);
		return "redirect:/academic_record/academic_records";
	}

}
