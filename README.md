How to set up the environment for the project.

1:	Install MySQL
	Go to http://dev.mysql.com/downloads/windows/installer/ and download the installer.
	Launch it.
	On the step "Choosing a Setup Type" select Custom (Because the installer is bugged) and click next.
	Add the following products:
		"MySql Servers" -> "MySQL Server" -> "MySQL Server 5.6" -> [the one corresponding to your OS architecture]
		"Applications" -> "MySQL Workbench" -> "MySQL Workbench 6.2" -> [the one corresponding to your OS architecture]
		"MySQL Connectors" -> "Connector/J" -> "Connector/J 5.1" -> "Connector/J 5.1.34 - x86"
	Then click next.
	On the step "Check Requirements" click execute, then click next.
	Click on Execute and wait for the 3 installations to complete.
	Click Next until you reach "Accounts and Roles".
	Define your default root password.
	Add the following user:
	{
		Username: soen341
		Host: localhost
		Role: DB Manager
		Password: 123456
	}
	Click Next.
	Untick "Configure MySQL Server as a Windows Service" and click next.
	Click Execute, allow the connection and then click Finish.
	Click Next and Finish.

2:	Setup Database
	Launch MySQL Workbench
	Setup a new connection (the plus symbol after "MySQL Connections")
	{
		Conection Name: localhost
	}
	Now double click on your connection.
	Click on "Data Import/Restore".
	Select "Import from Self-Contained File" and select the serenity.sql file, then click "Start Import".

3:	Setup Eclipse and Maven
	Download and extract "Eclipse IDE for Java Developers" from https://www.eclipse.org/downloads/
	Start eclipse.
	Go to "Help" -> "Eclipse Marketplace".
	Find "maven" and install "Maven Integration for Eclipse (Luna) 1.5.0".

4:	Git
	Download the git client here: http://sourceforge.net/projects/gitextensions/files/latest/download
	Install it, install all the required softwares and select "open-ssh" as ssh client.
	
	Start the program "Git Bash" and then type "ssh-keygen" and return. When asked to prompt something,
	just press return.
	
	Go to https://bitbucket.org/ and connect with your account (if you don't have one, create it and send me your nickname so I can add you to the project).
	Click on your avatar in the top right corner then on "Manage Account".
	Click on "SSH keys", then on "Add Key".
	Label it as you want, and put inside the key the content of the file "C:\Users\Me\.ssh\id_rsa.pub"
	
	Launch Git Extensions, then click on "Clone repository"
		Put "git@bitbucket.org:edmond_j/soen341.git" as "Repository to clone".
		Put the folder you want to put the source in as "Destination" (ex: "Documents\git")
	Click on clone,  a command prompt will open, type 'yes' and return.
	
5:	Open the project
	Start Eclipse and set as workspace the folder where you cloned the project (ex: "Documents\git\soen341").
	Close the start page.
	In the package explorer, right click and select "Import...".
	In the list, select "Existing Projects into Workspace".
	As the root directory, put the folder where you cloned the project (ex: "Documents\git\soen341").
	Select Prototype1 in the list and click on Finish.
	Open the file "pom.xml" and click on "Dependencies".
	Then click on "Manage...", select all the dependencies on the left side and the project on the right side, then click OK.
	Wait for the dependencies to be resolved.

And that should be it.
